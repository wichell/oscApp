/**
*
* 包装navigator
*/

let React = require("react-native")

let {
  Navigator,
  View
}=React


class NavigatorWrapper extends React.Component {
  constructor(props) {
    super(props)
  }

  render(){
    return (
      <Navigator
        initialRoute={{name: '', component: this.props.component, index:0}}
        configureScene={()=>{return Navigator.SceneConfigs.FloatFromBottom;}}
        renderScene={(route, navigator) => {
          const Component = route.component;
          return (
            <View style={{flex: 1}}>
              <Component navigator={navigator} route={route} {...route.passProps}/>
            </View>
          );
        }}/>
      );
  }
}

export default NavigatorWrapper;
