/**
* 收藏 列表 详情页
*/
'use strict'

var HTMLWebView = require('react-native-html-webview');

import Api from "../common/Api"
import Util from "../common/Util"

var React = require('react-native');
var {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Dimensions
}=React;


export default class Detail extends React.Component{
  constructor(props){
    super(props)
    this.state={
      software:{},
      show:false
    }
  }

  componentDidMount(){
    const that=this;
    const {objid}=this.props;
    console.log('objid='+objid);
    var url =[Api.software_detail,objid].join('/');
    Util.get(url,(data)=>{
      console.log('software_detail:'+JSON.stringify(data));
      that.setState({
        software:data.oschina.software,
        show:true,
      })
    })

  }

  render(){
    const data=this.state.software;
    return (
      <View style={[styles.flex_1,styles.flex_column]}>
        {this.state.show && data.title!==undefined?
        <ScrollView >
          <View>
            <Text>{data.title}</Text>
          </View>
          <View style={styles.flex_row}>
            <Text>{data.extensionTitle}</Text>
          </View>
          <HTMLWebView
            style={{flex:1}}
            html={data.body}
            makeSafe={true}
            autoHeight={true}
            />
        </ScrollView>
        : Util.loading}
      </View>
    );
  }
}

var styles=StyleSheet.create({
  flex_1:{
    flex:1,
    marginTop:5
  },
  flex_row:{
    flexDirection:'row',
  },
  flex_column:{
    flexDirection:'column',
  },
  header:{
    flexDirection:'row',
    height:30,
    marginTop:20,
    paddingTop:10,
    //backgroundColor:'#4ead7d',
  },
  headerLeft:{
    alignItems:'flex-start',
  },
  headerCenter:{
    alignItems:'center',
    paddingLeft:parseInt(Dimensions.get('window').width)/2-70,
  }
});
