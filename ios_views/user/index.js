'use strict';

import Util from "../common/Util"
import Api from "../common/Api"

import Login from "./login"
import Favorite from "./favorite"
import Config from "./config"

var React = require('react-native');

var {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  AsyncStorage,
}=React;


export default class MeApp extends React.Component {
  constructor(props) {
    super(props)

    this.state={
      user:{
        name:'点击头像登录',
        portrait:'',
        score:'0',//积分
        favoritecount:'0',//收藏
        followers: "0",//关注
        fans: "0"//粉丝

      },
      notice:{
        atmeCount:'0',
        msgCount:'0',
        newFansCount:'0',
        newLikeCount:'0',
        reviewCount:'0'
      }
    }

  }

  componentDidMount(){
    var that=this;
    AsyncStorage.getItem('uid',function(err,uid){
      if(!err && uid){
        AsyncStorage.getItem('key',function(err,key){
            var url=Api.my_information;
            var param={
              uid:uid,
              key:key,
            }
            Util.post(url,param,function(data){
              console.log(JSON.stringify(data));
              var user=data.oschina.user;
              var notice=data.oschina.notice;
              that.setState({
                user:{
                  name:user.name,
                  portrait:user.portrait,
                  score:user.score,//积分
                  favoritecount:user.favoritecount,//收藏
                  followers: user.followers,//关注
                  fans: user.fans//粉丝

                },
                notice:{
                  atmeCount:'0',
                  msgCount:'0',
                  newFansCount:'0',
                  newLikeCount:'0',
                  reviewCount:'0'
                }
              })
            })
        })
      }
    })
  }

  _loadPage(component,title){
    this.props.navigator.push({
      component:component,
      title: title
    })
  }

  render(){
    return (
      <ScrollView style={styles.userContainer}>

        <View style={styles.userInfo}>
          <TouchableOpacity onPress={this._loadPage.bind(this,<Login/>,'登录')}>
            <Image
              style={styles.avatar}
              source={this.state.user.portrait===''?require('image!default-portrait'):{uri:this.state.user.portrait}}>
            </Image>
          </TouchableOpacity>
          <Text  style={[styles.tag,{fontSize:18}]}>{this.state.user.name}</Text>
        </View>


        <View style={styles.flexRow}>
          <View style={styles.cell}>
            <TouchableOpacity>
              <View style={styles.cell}>
                <Text style={styles.tag}>积分</Text>
                <Text style={styles.tag}>{this.state.user.score}</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.cell}>
            <TouchableOpacity
              onPress={this._loadPage.bind(this,<Favorite/>,'收藏')}>
              <View style={styles.cell}>
                <Text style={styles.tag}>收藏</Text>
                <Text style={styles.tag}>{this.state.user.favoritecount}</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.cell}>
            <TouchableOpacity>
              <View style={styles.cell}>
                <Text style={styles.tag}>关注</Text>
                <Text style={styles.tag}>{this.state.user.followers}</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.cell}>
            <TouchableOpacity>
              <View style={styles.cell}>
                <Text style={styles.tag}>粉丝</Text>
                <Text style={styles.tag}>{this.state.user.fans}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.cell_1}>
              <Image
                style={styles.img}
                source={require('image!me-message')}>
              </Image>
              <Text style={styles.title}>消息</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.cell_1}>
              <Image
                style={styles.img}
                source={require('image!me-blog')}>
              </Image>
              <Text style={styles.title}>博客</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.item}>
          <TouchableOpacity>
            <View style={styles.cell_1}>
              <Image
                style={styles.img}
                source={require('image!me-team')}>
              </Image>
              <Text style={styles.title}>团队</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{marginTop:30}}>
          <TouchableOpacity
            onPress={this._loadPage.bind(this,<Config/>,'设置')}>
            <View style={styles.item}>
              <Text style={styles.title}>设置</Text>
            </View>
          </TouchableOpacity>
        </View>

      </ScrollView>

    );

  }
}


var styles=StyleSheet.create({
  userContainer:{
    backgroundColor:'#eeedf1'
  },
  avatar:{
    width:90,
    height:90,
    borderRadius:45,
    borderWidth:Util.pixel*2,
    borderColor:'#ffffff'
  },
  userInfo:{
    height:180,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#4ead7d'
  },
  flexRow:{
    flexDirection:'row',
    backgroundColor:'#4ead7d',
    borderTopWidth: Util.pixel,
    borderTopColor: '#ddd',
  },
  item:{
    flexDirection:'column',
    flex:1,
    height:40,
    justifyContent: 'center',
    borderTopWidth: Util.pixel,
    borderTopColor: '#ddd',
    backgroundColor:'#fff',
  },
  cell:{
    flex:1,
    height:40,
    alignItems:'center',
    justifyContent:'center',

  },
  tag:{
    color:'#ffffff'
  },
  cell_1:{
    flexDirection:'row'
  },
  img:{
    marginLeft:10
  },
  title:{
    marginLeft:15,
    marginTop:5,
  },
  font:{
    fontSize:15,
    marginLeft:5,
    marginRight:10,
  },

});
